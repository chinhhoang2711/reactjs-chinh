import React, { Component } from "react";
import Child from "./child";
import Pure from "./pure";

class LifeCycle extends Component {
  constructor(props) {
    super(props);
    console.log("constructor");
    //Hàm khởi tạo constructor cũng chạy 1 lần duy nhất

    this.state = {
      number: 0,
    };
  }

  UNSAFE_componentWillMount() {
    console.log("componentWillMount");
    //Chạy sau constructor
    //Chạy 1 lần
  }

  componentDidMount() {
    console.log("componentDidMount");
    //Chạy sau render
    //Gọi api fetch data
    //Chạy 1 lần duy nhất
  }

  componentDidUpdate() {
    console.log("componentDidUpdate");
    //Nó sẽ chạy khi State thay đổi.
  }

  shouldComponentUpdate(nextProps, nextState) {
    //sẽ cho 1 câu hỏi co nên cập nhật k , nó sẽ hỏi mình và mình đặt điều kiện cho nó
    console.log("shouldComponentUpdate", nextProps, nextState);
    if (nextState.number === 3) {
      return false;
    }
    return true;
  }

  //UNSAFE_componentWillReceiveProps
  UNSAFE_componentWillReceiveProps(nextProps) {
    console.log("componentWillReceiveProps", nextProps);
  }

  handleClick = () => {
    this.setState({
      number: this.state.number + 1,
    });
  };

  render() {
    console.log("render");
    return (
      <div>
        <h3>LifeCycle</h3>
        <br />
        <p>Number: {this.state.number}</p>

        <button className="btn btn-success" onClick={this.handleClick}>
          Click
        </button>

        <Child number={this.state.number}/>
        <Pure/>
      </div>
    );
  }
}

export default LifeCycle;
