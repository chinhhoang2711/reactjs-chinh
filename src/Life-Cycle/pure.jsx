import React, { PureComponent } from 'react';

class Pure extends PureComponent {
    render() {
        console.log("render-Pure");
        return (
            <div>
                <h3>Pure</h3>
            </div>
        );
    }
}

export default Pure;