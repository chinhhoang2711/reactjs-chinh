import React, { Component } from "react";

class Child extends Component {
    constructor(props) {
        super(props);
        console.log("constructor");
        //Hàm khởi tạo constructor cũng chạy 1 lần duy nhất
    
        this.state = {
          username: "Chinh",
        };
      }



    UNSAFE_componentWillReceiveProps(nextProps) {
    console.log("Child- componentWillReceiveProps", nextProps);
    //Nó sẽ chạy trong component con được từ Cha truyền qua cai Props
   
  }

  static getDerivedStateFromProps(nextProps, currentState){
      /**
       * thay thế cho componentWillReceiveProps
       */
      console.log("Child- getDerivedStateFromProps", nextProps, currentState);
      return {
          username: "CyberSoft"
      };
  }

  render() {
    console.log("render-child");
    return (
      <div>
        <br />
        <h3>Child</h3>
      </div>
    );
  }
}

export default Child;
