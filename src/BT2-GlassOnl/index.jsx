import React, { Component } from "react";
import "./style.css";
import {
  Container,
  Card,
  Row,
  Col,
  Button,
  CardDeck,
  CardImg,
} from "react-bootstrap";

const dataGlass = [
  {
    id: 1,
    price: 30,
    name: "GUCCI G8850U",
    url: "./glassesImage/v1.png",
    desc:
      "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  },

  {
    id: 2,
    price: 50,
    name: "GUCCI G8759H",
    url: "./glassesImage/v2.png",
    desc:
      "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  },

  {
    id: 3,
    price: 30,
    name: "DIOR D6700HQ",
    url: "./glassesImage/v3.png",
    desc:
      "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  },

  {
    id: 4,
    price: 30,
    name: "DIOR D6005U",
    url: "./glassesImage/v4.png",
    desc:
      "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  },

  {
    id: 5,
    price: 30,
    name: "PRADA P8750",
    url: "./glassesImage/v5.png",
    desc:
      "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  },

  {
    id: 6,
    price: 30,
    name: "PRADA P9700",
    url: "./glassesImage/v6.png",
    desc:
      "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  },

  {
    id: 7,
    price: 30,
    name: "FENDI F8750",
    url: "./glassesImage/v7.png",
    desc:
      "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  },

  {
    id: 8,
    price: 30,
    name: "FENDI F8500",
    url: "./glassesImage/v8.png",
    desc:
      "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  },

  {
    id: 9,
    price: 30,
    name: "FENDI F4300",
    url: "./glassesImage/v9.png",
    desc:
      "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  },
];

class GlassAppOnline extends Component {
  state = {
    name: "GUCCI G8850U",
    url: "./glassesImage/v1.png",
    desc:
      "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  };

  changeGlass = (id) => () => {
    const product = this.dataGlass.find((item) => item.id === id);
    const { name, url, desc } = product;
    this.setState({
      name: name,
      url: url,
      desc: desc,
    });
  };

  renderDislay = () => {
    return (
      <div className="glass-content">
        <CardDeck className="card-Deck">
          <Card className="model-card">
            <Card.Img
              className="w-100"
              src="./glassesImage/model.jpg"
              alt="model"
            />
            <Card.ImgOverlay className="p-0">
              <div className="overplay-header">
                <div className="overplay-glass">
                  <img className="w-100" src={this.state.url} alt="glass" />
                </div>
              </div>
              <div className="card-footer">
                <Card.Title className="card-title">
                  {this.state.name}
                </Card.Title>
                <Card.Text className="card-text">{this.state.desc}</Card.Text>
              </div>
            </Card.ImgOverlay>
          </Card>

          <Card className="model-card">
            <Card.Img src="./glassesImage/model.jpg" alt="model" />
          </Card>
        </CardDeck>
      </div>
    );
  };

  renderClass = () => {
    dataGlass.map((item, index) => {
      console.log(item);
      const { id, url } = item;
      return (
        <Col key={index}>
          <Button
            variant="light outline-secondary"
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
            onClick={() => this.changeGlass(id)}
          >
            <img
              src={url}
              style={{
                width: "70px",
                height: "30px",
                display: "block",
              }}
              alt="kinh sịn "
            />
          </Button>
        </Col>
      );
    });
  };

  render() {
    return (
      <div className="home-bg container-fluid">
        <img className="img-glass" src="./glassesImage/background.jpg" alt="" />
        <div className="status-glass">
          <h2 className="text-white">TRY GLASSES APP ONLINE</h2>
        </div>

        {this.renderDislay()}

        <div className="slecGlass">
          <Row
            style={{
              width: "70%",
            }}
          >
            {this.renderClass()}
          </Row>
        </div>
      </div>
    );
  }
}

export default GlassAppOnline;
