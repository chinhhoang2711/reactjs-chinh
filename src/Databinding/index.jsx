import React, { Component } from "react";

class Databinding extends Component {
  name = "Hoàng Công Chính";
  isMale = true;

  checkGender() {
    if (this.isMale) return "Male";
    return "Female";
  }

  //TH1: hàm bình thường, không tham số:
  //Gọi hàm sự kiện thì k bỏ cặp ngoặc
  showMessage() {
    alert("Xin Chào mọi người");
  }

  //TH2: hàm nhận thông số đầu vào:
  // cách 1: truyền 1 function ẩn danh vào bọc lại
  // -------------
  // cách 2: Closure (bên dứi)
  showMessageWithParams = (message) => () => {
    console.log(message);
  };

  //TH3: hàm có con trỏ this
  //Bị thay đổi ngữ cảnh thì bỏ () = >, function ẩn danh
  showMessageWithThis = () => {
    console.log(this.name);
  };

  render() {
    let age = 25;
    //Biến cục bộ ko cần gọi this.
    return (
      <div>
        <button onClick={this.showMessage}>Show Message</button>

        <button onClick={this.showMessageWithThis}>
          Show Message With This
        </button>

        <button onClick={this.showMessageWithParams("Chính")}>
          Show Message With Params
        </button>

        <h1>Demo Databinding</h1>
        <h3>Author: {this.name}</h3>
        {/* <h3>Gender: {this.checkGender()}</h3> */}
        <h3>Gender: {this.isMale ? "Male" : "Female"}</h3>
        <h3>Age: {age}</h3>
      </div>
    );
  }

  // Kiểm tra giới tính ( điều kiện ), nếu đúng là Male : k phải là Female
}

export default Databinding;
