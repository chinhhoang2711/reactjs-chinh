import React, { Component } from "react";
import Header from "../Header";
import ProductList from "../ProductList";
import Detail from "../Detail";
import Cart from "../Cart";

class HomeShopping extends Component {
  products = [
    {
      id: "sp_2",
      name: "Note 7",
      price: "500",
      screen: "Full HD+",
      backCamera: "backCamera_2",
      frontCamera: "frontCamera_2",
      img:
        "https://www.xtmobile.vn/vnt_upload/product/01_2018/thumbs/600_note_7_blue_600x600.png",
      desc:
        "The Galaxy Note7 comes with a perfectly symmetrical design for good reason",
    },
    {
      id: "sp_3",
      name: "Vivo",
      price: "300",
      screen: "Full HD+",
      backCamera: "backCamera_3",
      frontCamera: "frontCamera_3",
      img: "https://www.gizmochina.com/wp-content/uploads/2019/11/Vivo-Y19.jpg",
      desc:
        "A young global smartphone brand focusing on introducing perfect sound quality",
    },
    {
      id: "sp_4",
      name: "Blacberry",
      price: "400",
      screen: "Super Amoled",
      backCamera: "backCamera_4",
      frontCamera: "frontCamera_4",
      img:
        "https://cdn.tgdd.vn/Products/Images/42/194834/blackberry-keyone-3gb-600x600.jpg",
      desc:
        "BlackBerry is a line of smartphones, tablets, and services originally designed",
    },
  ];

  state = {
    selectedProduct: null,
    cart: [],
    totalCartItem: 0,
    totalAmount: 0,
  };

  //tạo biến rỗng xuống kia lấy dữ liệu

  //truyền từ cha xuống con, qua từng cấp
  getProduct = (dataFromChild) => {
    console.log(dataFromChild);
    //dataFromChild là dữ liệu từ th con ProductItem truyền lên th Cha (HOme)
    this.setState({
      selectedProduct: dataFromChild,
      //   sửa lại thuộc tính nào thì bỏ vào setSate
    });
  };

  putToCart = (prod) => {
    console.log(prod);

    //tạo 1 cart copy, để sửa lên nó rồi gán lại th Cart chính
    // (copy lại dùng ...)
    const cloneCart = [...this.state.cart];

    const cartItem = {
      product: prod,
      quantily: 1,
    };

    //Vì thêm có k cộng số lượng lại, Kiểm tra sp đã có trong giỏ hàng chưa ( sửa là sửa ở mảng copy)
    //truyền item vào (item ở đây đang là đối tượng sp trong giỏ hàng)
    const index = cloneCart.findIndex((item) => {
      return item.product.id === prod.id;
    });

    //nếu chưa tồn tại thì push
    if (index === -1) {
      cloneCart.push(cartItem);
    }
    //có rồi, thì tăng số lượng
    else {
      cloneCart[index].quantily++;
    }

    this.setState({
      cart: cloneCart,
      totalCartItem: this.state.totalCartItem + 1,
      // totalAmount: this.state.totalAmount,
    });
  };

  //Xóa sp trong giỏ hàng
  removeCartItem = (id) => {
    console.log(id);
    const cloneCart = [...this.state.cart];
    const index = cloneCart.findIndex((item) => {
      if (item.id === id) {
        return true;
      }
    });
    cloneCart.splice(index, 1);
    // this.setState({cloneCart, indx})
  };

  render() {
    return (
      <div>
        <Header total={this.state.totalCartItem} />
        <ProductList
          getProduct={this.getProduct}
          data={this.products}
          putToCart={this.putToCart}
        />
        {this.state.selectedProduct && (
          <Detail item={this.state.selectedProduct} />
        )}
        <Cart
          dataCart={this.state.cart}
          amount={this.state.totalAmount}
          removeCartItem={this.removeCartItem}
        />
      </div>
    );
  }
}

export default HomeShopping;
