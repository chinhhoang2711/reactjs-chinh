import React, { Component } from "react";
import classes from "./style.module.css";

class ProductItem extends Component {
  //dùng state để render lại , thay đổi giao diện
  state = {
    isShow: true,
  };

  toogleDescription = () => {
    // this.state.isShow = !this.state.isShow;
    this.setState({
      isShow: !this.state.isShow,
    });
  };

  render() {
    const { name, img, desc } = this.props.item;
    // Kỉ thuật bóc tách phần tử
    return (
      <div className="card">
        <img src={img} alt="product" className={classes.productImg} />
        <div className="card-body">
          <h3>{name}</h3>

          {/* nếu là false thì ẩn mà true thì hiện  */}
          {this.state.isShow && <p>{desc}</p>}
          {/* {this.isShow ? <p>{desc}</p> : null} */}
          {/* truyền tham số là nguyên cái dữ liệu item (hiện tại đang là this.props.item ) */}
          {/* hàm có tham số đầu vào thì bọc nó lại bằng hàm cha, error function  */}
          <button
            onClick={() => this.props.getProduct(this.props.item)}
            className="btn btn-success mx-2"
          >
            Xem chi tiết
          </button>

          {/* có tham số nên bọc thằng cha lại */}
          <button
            onClick={this.toogleDescription}
            className="btn btn-danger mx-2"
          >
            Ẩn mô tả
          </button>

          {/* có tham số nên bọc thằng cha lại */}
          <button
            onClick={() => this.props.putToCart(this.props.item)}
            className="btn btn-warning mt-2 ml-2"
          >
            Thêm vào giỏ hàng
          </button>
        </div>
      </div>
    );
  }
}

export default ProductItem;
