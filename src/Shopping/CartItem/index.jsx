import React, { Component } from "react";
import DeleteIcon from "@material-ui/icons/Delete";

class CartItem extends Component {
  render() {
    // console.log(this.props.item.product.item);
    const { img, name, price } = this.props.item.product;
    const { quantily } = this.props.item;

    return (
      <tr>
        <td>
          <img style={{ width: 100, height: 80 }} src={img} />
        </td>
        <td style={{ fontSize: 25 }}>{name}</td>
        <td>{price} $</td>
        <td>
          {quantily}
          <div className="btn-group ml-3">
            <button className="btn btn-info border-right">-</button>
            <button className="btn btn-info border-left">+</button>
          </div>
        </td>
        <td>{quantily * price} $</td>
        <td>
          <button
            onClick={() => this.props.removeCartItem(this.props.item.product.id)}
            className="btn btn-danger rounded"
          >
            <DeleteIcon />
          </button>
        </td>
      </tr>
    );
  }
}

export default CartItem;
