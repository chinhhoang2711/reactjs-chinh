import React, { Component } from "react";
import UserItem from "../UserItem";
import { connect } from "react-redux";

class Users extends Component {
  //lấy từ Store về rồi thì render map nó ra
  renderUserItem = () => {
    return this.props.users.map((item, index) => {
      return <UserItem key={index} item={item} />;
    });
  };

  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>Name</th>
              <th>Username</th>
              <th>Email</th>
              <th>Phone Number</th>
              <th>Type</th>
            </tr>
          </thead>
          <tbody>{this.renderUserItem()}</tbody>
        </table>
      </div>
    );
  }
}

//đặt tên gì cũng đc
//return về 1 cái oject {}
const mapStateToProps = (state) => {
  return {
    //Muốn lấy dữ liệu gì thì lấy State {Chấm đúng cái tên cần lấy}
    //chuyển về 1 cái props, đặt tên gì cũng đc
    //cuối cùng bỏ thằng tên hàm vào Connect để nó chạy
    users: state.userList,
  };
};

export default connect(mapStateToProps)(Users);
