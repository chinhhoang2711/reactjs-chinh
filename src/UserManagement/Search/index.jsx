import React, { Component } from "react";

class Search extends Component {
  render() {
    return <input type="text" placeholder="Search" className="form-control mb-3 w-50" />;
  }
}

export default Search;
