import React, { Component } from "react";
import { connect } from "react-redux";

class UserItem extends Component {
  handleDeleteUser = () => {
    this.props.dispatch({
      type: "DELETE_USER",
      payload: this.props.item.username,
    });
  };

  openForm = () => {
    this.props.dispatch({
      //Vì có 1 cái cũ cùng chức năng rồi nên dùng lại SHOW_FORM cái cữ luôn
      // type: "OPEN_FORM",
      type: "SHOW_FORM",
    });
  };

  handleEditUser = () => {
    this.openForm();
    this.props.dispatch({
      type: "SELECT_USER",
      payload: this.props.item,
      //gửi nguyên đối tượng lên Store
    });
  };

  render() {
    const { name, username, email, phoneNumber, type } = this.props.item;
    return (
      <tr>
        <td>{name}</td>
        <td>{username}</td>
        <td>{email}</td>
        <td>{phoneNumber}</td>
        <td>{type}</td>
        <td>
          <button onClick={this.handleEditUser} className="btn btn-info mr-2">
            Edit
          </button>
          <button onClick={this.handleDeleteUser} className="btn btn-danger">
            Delete
          </button>
        </td>
      </tr>
    );
  }
}

//Tạo thêm 1 reducer mới là slectedUSer trên Store (index.js)
export default connect()(UserItem);
