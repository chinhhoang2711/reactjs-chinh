import { Fade } from "@material-ui/core";

//Giá trị ban đầu
let initailState = [
  {
    id: 1,
    name: "Dinh Phuc Nguyen",
    username: "dpnguyen",
    email: "dpnguyen@gmail.com",
    phoneNumber: "1123123213",
    type: "VIP",
  },
  {
    id: 2,
    name: "Nguyen Dinh Phuc",
    username: "nguyendp",
    email: "nguyendp@gmail.com",
    phoneNumber: "1123123213",
    type: "VIP",
  },
];

const reducer = (state = initailState, action) => {
  //bóc type ra từ Form dispatch
  const { type, payload } = action;
  //Bóc thuộc tính payload ra rồi dưới sài thay vì action.payload
  switch (type) {
    case "CREATE_USER":
      state.push(payload);
      console.log(state);
      //Shallow comparation
      //copy ra một cái [] mới và return nó ra, thì redux nó mới nhận diện được sự thay đổi
      return [...state];
    case "DELETE_USER": {
      //findIndex duyệt mảng từ đầu tới đuôi nhận vào 1 tham số callback function, giống th map
      //Bọc lại bằng một cặp {} để dùng trùng tên ,vì 2 th kia const
      const indexUser = state.findIndex((item, index) => {
        return item.username === payload;
      });
      state.splice(indexUser, 1);
      console.log(indexUser);
      return [...state];
    }
    case "UPDATE_USER": {
      const indexUser = state.findIndex((item, index) => {
        return item.username === payload.username;
      });
      state[indexUser] = payload;
      return [...state];
    }
    default:
      return state;
  }
  //Vào gì ra đó
};

export default reducer;
