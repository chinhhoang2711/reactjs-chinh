//KHai báo lại giá trị ban đầu của biến "isShow"
let initialState = false;

//reducer là 1 cái hàm nhận giá trị hiện tại
//state hiện tại ở đây đang là cái isShow (giá trị hiện tại của dữ liệu)
//Bắt buộc luôn luôn return về 1 giá trị
const reducer = (state = initialState, action) => {
  const { type } = action;
  switch (type) {
    case "SHOW_FORM":
      state = true;
      return state;
      
    //Thay vì gán rồi return thì return thẳng luôn

    // return true;

    //Sửa hay ko sửa thì return giá trị mới (immutable: tính Bất Biến, chúng ta k đc sửa cái giá trị chúng ta tạo ra )
    case "CLOSE_FORM":
      state = false;
      return state;
    //dùng lại cái SHOW_FORM vì cùng chức năng
    // case "OPEN_FORM":
    //   state = true;
    //   return state;
    default:
      return state;
  }
};

export default reducer;
