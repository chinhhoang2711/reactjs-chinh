//Giá trị ban đầu là null k phải mảng rỗng []

let initialState = null;

//có thể bóc trực tiếp trên
// const reducer = (state = initialState, { type, payload}){}
const reducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case "SELECT_USER":
      //   state = payload;
      //object cũng giống như arr khi trả về phải bắt buộc trả về bản copy của object đó
      //tại sao phải sét payload = state rồi return về state. Giờ return về thẳng payload, đặc biệt k cần phải copy {}
      //   return {...state};
      return payload;
    default:
      return state;
  }
};

export default reducer;

// export ra index.js  đặt tên
