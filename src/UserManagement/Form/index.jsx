import React, { Component } from "react";
import { connect } from "react-redux";
class Modal extends Component {
  //Muốn sửa thì .dispatch , gửi 1 action lên Store
  handleCloseForm = () => {
    this.props.dispatch({
      type: "CLOSE_FORM",
    });
  };

  state = {
    user: {
      username: "",
      name: "",
      email: "",
      phoneNumber: "",
      type: "",
    },
  };

  handleChange = (e) => {
    this.setState({
      //lấy name làm key, đặt mỗi cái input 1 cái name
      user: { ...this.state.user, [e.target.name]: e.target.value },
    });
  };

  //chặn nó load lại trang khi nhấn Submit
  handleSubmit = (e) => {
    e.preventDefault();
    //Mình lụm dữ liệu selectedUSer trên Store xuống nó đang là this.props.user
    if (this.props.user) {
      //code update
      this.props.dispatch({
        type: "UPDATE_USER",
        //gửi hết thông tin lên
        payload: this.state.user,
      });
    } else {
      //Đẩy dữ liệu lên Store .dispatch
      this.props.dispatch({
        type: "CREATE_USER",
        //Gửi nguyên đối tượng lụm từ form ra đẩy lên Store
        payload: this.state.user,
      });
    }

    //ko nên dùng Document trong React
    //nhấn nút này thì gọi luôn cái nút Cancel
    this.refs.btnCancel.click();
    // this.handleCloseForm(); //làm này đơn giản nhanh hơn
  };

  render() {
    return (
      <div
        style={{
          background: "rgba(0,0,0,0.7)",
          position: "absolute",
          top: 0,
          left: 0,
          width: "100%",
          height: "100%",
          display: "flex",
          alignItems: "center",
        }}
      >
        <div className="bg-white w-50 mx-auto px-5 pb-3 rounded ">
          <h1 className="text-center display-4 m-0">Form User</h1>
          <form className="text-white" onSubmit={this.handleSubmit}>
            <div className="form-group">
              <label className="text-dark">Username</label>
              <input
                onChange={this.handleChange}
                name="username"
                value={this.state.user.username}
                type="text"
                className="form-control"
              />
            </div>
            <div className="form-group">
              <label className="text-dark">Name</label>
              <input
                onChange={this.handleChange}
                name="name"
                value={this.state.user.name}
                type="text"
                className="form-control"
              />
            </div>
            <div className="form-group">
              <label className="text-dark">Email</label>
              <input
                onChange={this.handleChange}
                name="email"
                value={this.state.user.email}
                type="text"
                className="form-control"
              />
            </div>
            <div className="form-group">
              <label className="text-dark">Phone Number</label>
              <input
                onChange={this.handleChange}
                name="phoneNumber"
                value={this.state.user.phoneNumber}
                type="text"
                className="form-control"
              />
            </div>
            <div className="form-group">
              <label className="text-dark">Type</label>
              <select
                className="form-control"
                onChange={this.handleChange}
                name="type"
                value={this.state.user.type}
              >
                <option>Chọn</option>
                <option>USER</option>
                <option>VIP</option>
              </select>
            </div>
            <button
              data-dismiss="modal"
              type="submit"
              className="btn btn-success"
            >
              Submit
            </button>
            <button
              ref="btnCancel"
              onClick={this.handleCloseForm}
              type="button"
              className="btn btn-info ml-3"
            >
              Cancel
            </button>
          </form>
        </div>
      </div>
    );
  }

  //K cần gọi nó tự chạy, sau render
  componentDidMount() {
    this.props.user && this.setState({ user: this.props.user });
  }

  // sét lại cho thành null , 2 thằng nay chỉ chạy 1 lần duy nhất
  componentWillUnmount() {
    this.props.dispatch({
      //dùng lại action
      type: "SELECT_USER",
      payload: null,
    });
  }
}

const mapStateToProps = (state) => {
  //State là object chứa tất cả dữ liệu trên Store
  return {
    //đặt tên gì cũng đc (vế trái)
    user: state.selectedUser,
  };
};

//Tất cả dữ liệu lưu trên Store đều là State hết, lụm nó về sài sẽ bị thay đổi nên phải render lại
export default connect(mapStateToProps)(Modal);

//xây dựng 2 chiều: sét mấy cái input có value={this.state.user.username}
