import React, { Component } from "react";
import Search from "../Search";
import Users from "../UserList";
import Modal from "../Form";
import { connect } from "react-redux";

class Home extends Component {
  handleAddUser = () => {
    //CHỉnh sửa isShow trên Store = true
    //Muốn sửa cái isShow thì: phải connect rồi thì .dispatch
    //Chưa có action nên phải tạo ra action
    //Action có 1 thuộc tính bắt buộc là "Type"
    const action = {
      type: "SHOW_FORM",
    };
    //Khi Store đổi component nào lấy về sài thì thay đổi theo
    this.props.dispatch(action);
    //thằng form reducer là th tiếp nhận nó để nó xử lí
  };

  render() {
    return (
      <div className="container">
        <h1 className="display-4 text-center my-3">User Management</h1>
        <div className="d-flex justify-content-between align-items-center">
          <Search />
          <button onClick={this.handleAddUser} className="btn btn-success">
            Add User
          </button>
        </div>
        <Users />

        {this.props.isShowModal && <Modal />}
      </div>
    );
  }
}

//Để lấy về sài từ Store
const mapStateToProps = (state) => {
  return {
    isShowModal: state.isShow,
    // lấy giá trị từ Store về (state.isShow) nhưng k sài liền đc, mà chúng ta phải chuyển nó thành một cái Props ( tên Props. Giá trị)
  };
};

export default connect(mapStateToProps)(Home);
