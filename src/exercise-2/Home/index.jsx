import React from "react";
import Header from "../Header";
import Carousel from "../Carousel";
import Intro from "../Intro";
import Cards from "../Cards";
import Footer from "../Footer";

class Home extends React.Component {
  render() {
    return (
      <div>
        <Header />
        <Carousel />
        <div className="container">
          <Intro />
          <Cards />
        </div>
        <Footer />
      </div>
    );
  }
}

export default Home;
