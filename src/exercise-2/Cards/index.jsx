import React from "react";
import CardItem from "../Carditem";

import "./style.css";

class Cards extends React.Component {
  render() {
    return (
      <div className="row">
        <div className="col-4">
          <CardItem />
        </div>
        <div className="col-4">
          <CardItem />
        </div>
        <div className="col-4">
          <CardItem />
        </div>
      </div>
    );
  }
}

export default Cards;
