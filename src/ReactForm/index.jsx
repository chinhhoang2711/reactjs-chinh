import React, { Component } from "react";

class ReactForm extends Component {
  state = {
    user: {
      userName: "",
      fullName: "",
      email: "",
      phone: "",
      type: "",
    },
  };

  handleChange = (e) => {
    console.log("input change", e.target.value, e.target.name);
    this.setState({
      user: { ...this.state.user, [e.target.name]: e.target.value },
    });
  };

  //Bất cứ hàm handle sự kiện nào đều có tham số truyền vào là e (event)
  handleSubmit = (e) => {
    e.preventDefault();
    // e.preventDefault() dùng để chặn nó submit lại ko cho nó load lại trang
    console.log(this.state.user);
  };

  handleSetValue = () => {
    const user = {
      userName: "trunghieu",
      fullName: "dang trung hieu",
      email: "dangtrunghieu147@gmail.com",
      phone: "0334643124",
      type: "VIP",
    };
    this.setState({
      user: user,
    });
  };

  render() {
    return (
      <form className="w-50 mx-auto mt-5" onSubmit={this.handleSubmit}>
        <div className="form-group">
          <label>Username</label>
          <input
            value={this.state.user.userName}
            onChange={this.handleChange}
            name="userName"
            type="text"
            className="form-control"
          />
        </div>
        <div className="form-group">
          <label>Name</label>
          <input
            value={this.state.user.fullName}
            onChange={this.handleChange}
            name="fullName"
            type="text"
            className="form-control"
          />
        </div>
        <div className="form-group">
          <label>Email</label>
          <input
            value={this.state.user.email}
            onChange={this.handleChange}
            name="email"
            type="text"
            className="form-control"
          />
        </div>
        <div className="form-group">
          <label>Phone Number</label>
          <input
            value={this.state.user.phone}
            onChange={this.handleChange}
            name="phone"
            type="text"
            className="form-control"
          />
        </div>
        <div className="form-group">
          <label>Type</label>
          <select
            className="form-control"
            onChange={this.handleChange}
            value={this.state.user.type}
          >
            <option>USER</option>
            <option>VIP</option>
          </select>
        </div>
        <button type="submit" className="btn btn-success">
          Submit
        </button>
        {/* Muốn nó ko nhận nút subbmit thì đổi type nó thành button rồi gắn onclick */}
        <button
          type="button"
          onClick={this.handleSetValue}
          className="btn btn-info ml-3"
        >
          Set Value
        </button>
      </form>
    );
  }
}

export default ReactForm;
