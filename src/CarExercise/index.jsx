import React, { Component } from "react";
import redCar from "./img/red-car.jpg";
import silverCar from "./img/silver-car.jpg";
import blackCar from "./img/black-car.jpg";
import steelCar from "./img/steel-car.jpg";

class CarExercise extends Component {
  state = {
    carImg: redCar,
    // a ,b truyền thêm vào cho vui
    // 1 component có thể chứa nhiều cái state
    a: 1,
    b: 2,
  };

  // dính TH2 và TH3 , bọc lại
  // đê đổi component phải setState lại
  changeColor = (img) => () => {
    this.setState({
      carImg: img,
      // xuống state chỉ cập nhật lại thằng cần đổi là carImg
      //State sẽ cho chạy 2 hàm, cái sau là 1 cái call back function
    }, () => {
      console.log(this.state.carImg);
    });
    
  };

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-6 py-4">
            <h4>Please choose your favorite about cars's color </h4>
            <img className="w-100" src={this.state.carImg} alt="car" />
          </div>
          <div className="col-6">
            <h3 className="py-4">Change Color</h3>
            <button
              onClick={this.changeColor(redCar)}
              className="btn btn-danger mx-2"
            >
              Red Color
            </button>
            <button
              onClick={this.changeColor(silverCar)}
              className="btn btn-secondary mx-2"
            >
              Silver Color
            </button>
            <button
              onClick={this.changeColor(blackCar)}
              className="btn btn-dark mx-2"
            >
              Black Color
            </button>
            <button
              onClick={this.changeColor(steelCar)}
              className="btn btn-light mx-2"
            >
              Steel Color
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default CarExercise;
