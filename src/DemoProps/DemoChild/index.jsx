import React, { Component } from "react";
import DemoGrandChild from "../DemoGrandChild";

class DemoChild extends Component {
  render() {
    console.log(this.props.item);
    return (
      <div className="bg-success text-white p-4">
        <h1>Demo Child component</h1>
        <h4 className="text-warning">Name: {this.props.item.name}</h4>
        <h4 className="text-danger">Age: {this.props.item.age}</h4>

        <DemoGrandChild title={this.props.item} />
      </div>
    );
  }
}

// oneway binding vs 2way binding
// REACT là ONEWAY

export default DemoChild;
