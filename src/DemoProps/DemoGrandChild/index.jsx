import React, { Component } from "react";

class DemoGrandChild extends Component {
  render() {
    return (
      <div className="bg-warning text-white p-3">
        <h1>Demo Grand Child</h1>
        <h4 className="text-success">Name: {this.props.title.name}</h4>
        <h4 className="text-dark">Age: {this.props.title.age}</h4>
      </div>
    );
  }
}

export default DemoGrandChild;
