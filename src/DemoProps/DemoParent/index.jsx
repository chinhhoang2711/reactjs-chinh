import React, { Component } from "react";
import DemoChild from "../DemoChild";

class DemoParent extends Component {
  student = {
    name: "Chính",
    age: 15,
  };
  render() {
    return (
      <div className="bg-info text-white p-4">
        <h1>Demo Parent component</h1>
        <DemoChild item={this.student} />
      </div>
    );
  }
}

export default DemoParent;
