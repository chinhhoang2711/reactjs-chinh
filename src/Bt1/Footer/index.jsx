import React, { Component } from "react";

class Footer extends Component {
  render() {
    return (
      <section className="container-fluid pt-5 pb-5 bg-dark">
        <h3 className="text-center text-white">PROMOTION</h3>
        <div className="container bg-light pt-5 pb-5">
          <div className="row">
            <div className="col-xs-12 col-sm-12 col-md-4">
              <div className="container">
                <img className="w-100" src="./img/promotion_1.png" alt="" />
              </div>
            </div>
            <div className="col-xs-12 col-sm-12 col-md-4">
              <div className="container">
                <img className="w-100" src="./img/promotion_2.png" alt="" />
              </div>
            </div>
            <div className="col-xs-12 col-sm-12 col-md-4">
              <div className="container">
                <img className="w-100" src="./img/promotion_3.jpg" alt="" />
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Footer;
