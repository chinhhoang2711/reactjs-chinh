import React, { Component } from "react";

const data = [
  {
    id: "sp_1",
    name: "iphoneX",
    price: "30.000.000 VNĐ",
    screen: "screen_1",
    backCamera: "backCamera_1",
    frontCamera: "frontCamera_1",
    img:
      "https://sudospaces.com/mobilecity-vn/images/2019/12/iphonex-black.jpg",
    desc:
      "iPhone X features a new all-screen design. Face ID, which makes your face your password",
  },
  {
    id: "sp_2",
    name: "Note 7",
    price: "20.000.000 VNĐ",
    screen: "screen_2",
    backCamera: "backCamera_2",
    frontCamera: "frontCamera_2",
    img:
      "https://www.xtmobile.vn/vnt_upload/product/01_2018/thumbs/600_note_7_blue_600x600.png",
    desc:
      "The Galaxy Note7 comes with a perfectly symmetrical design for good reason",
  },
  {
    id: "sp_3",
    name: "Vivo",
    price: "10.000.000 VNĐ",
    screen: "screen_3",
    backCamera: "backCamera_3",
    frontCamera: "frontCamera_3",
    img: "https://www.gizmochina.com/wp-content/uploads/2019/11/Vivo-Y19.jpg",
    desc:
      "A young global smartphone brand focusing on introducing perfect sound quality",
  },
  {
    id: "sp_4",
    name: "Blacberry",
    price: "15.000.000 VNĐ",
    screen: "screen_4",
    backCamera: "backCamera_4",
    frontCamera: "frontCamera_4",
    img:
      "https://cdn.tgdd.vn/Products/Images/42/194834/blackberry-keyone-3gb-600x600.jpg",
    desc:
      "BlackBerry is a line of smartphones, tablets, and services originally designed",
  },
];

class ProductList extends Component {
  renderSmartPhone = () => {
    return data.map((item, index) => {
      return (
        <div className="col-xs-12 col-sm-6 col-md-6 col-lg-3" >
          <div className="container">
            <div className="card bg-light">
              <img
                src={item.img}
                alt="smartphone"
                style={{ maxWidth: "100%", height: 200 }}
              />
              <div className="card-body text-center">
                <h4 className="card-title text-center">{item.name}</h4>
                <p className="card-text">{item.desc}</p>
                <a href="" className="btn btn-primary mx-3">
                  Detail
                </a>
                <a href="" className="btn btn-danger">
                  Cart
                </a>
              </div>
            </div>
          </div>
        </div>
      );
    });
  };

  render() {
    return (
      <div className="bg-dark">
        <h3 className="pt-4 pb-2 text-center text-white">BEST SMARTPHONE</h3>
        <div className="container">
          <div className="row">{this.renderSmartPhone()}</div>
        </div>
      </div>
    );
  }
}

export default ProductList;
