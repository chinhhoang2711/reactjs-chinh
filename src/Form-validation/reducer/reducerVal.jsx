import { SUBMIT, EDIT } from "./constant";

let initialState = {
  listUser: [
    {
      id: "1",
      manv: "1234",
      tennv: "nguyen",
      email: "dpnguyen53@gmail.com",
    },
    {
      id: "2",
      manv: "2345",
      tennv: "hieu",
      email: "hieu53@gmail.com",
    },
  ],
  userEdit: null,
};

const formValidationReducer = (state = initialState, action) => {
  switch (action.type) {
    case SUBMIT:
      const index = state.listUser.findIndex((user) => {
        return user.id === action.payload.id;
      });
      let listUser = [...state.listUser];
      if (index !== -1) {
        //UPDATE
        listUser[index] = action.payload;
      } else {
        //ADD
        const userAdd = { ...action.payload };
        userAdd.id = Math.random();
        listUser = [...state.listUser, userAdd];
      }
      state.listUser = listUser;
      return { ...state };

    case EDIT:
      state.userEdit = action.payload;
      return { ...state };
    default:
      return { ...state };
  }
};

export default formValidationReducer;
