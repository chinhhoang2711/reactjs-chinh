import React, { Component } from "react";
import { connect } from "react-redux";

class ListUser extends Component {
  handleEditUser = () => {
    this.props.dispatch({
      type: "EDIT",
      payload: this.props.item,
    });
  };

  renderData = () => {
    return this.props.listData.map((item, index) => {
      return (
        <tbody key={index}>
          <td>{item.manv}</td>
          <td>{item.tennv}</td>
          <td>{item.email}</td>
          <td>
            <button onClick={this.handleEditUser} className="btn btn-info">Edit</button>
          </td>
        </tbody>
      );
    });
  };

  render() {
    return (
      <div>
        {console.log(this.props.listData)}
        <table className="table mt-5">
          <thead>
            <tr>
              <th>Mã NV</th>
              <th>Tên NV</th>
              <th>Email</th>
              <th></th>
            </tr>
          </thead>
          {this.renderData()}
        </table>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    listData: state.formValidationReducer.listUser,
  };
};

// const mapDispatchToProps = (dispatch) => {
//   return {
//     edit: (user) => {
//       dispatch(actEdit(user));
//     },
//   };
// };

export default connect(mapStateToProps)(ListUser);
