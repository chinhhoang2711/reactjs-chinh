import React, { Component } from "react";
import ListUser from "./list-user";
import { connect } from "react-redux";

class FormValidation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      values: {
        id: "",
        manv: "",
        tennv: "",
        email: "",
      },
      errors: {
        manv: "",
        tennv: "",
        email: "",
      },
      manvValid: false,
      tennvValid: false,
      emailValid: false,
      formValid: false,
    };
  }

  //values và error : tỷ lệ nghịch, console log ra xem

  handleOnchange = (e) => {
    const { name, value } = e.target;
    this.setState(
      {
        values: { ...this.state.values, [name]: value },
      },
      () => {
        console.log(this.state);
      }
    );
  };

  handleErrors = (e) => {
    const { name, value } = e.target;
    let message = "";
    // if (value === "") {
    //   message = "Vui lòng nhập thông tin";
    // } else {
    //   message = "";
    // }

    //toán tử 3 ngôi:
    // điều kiện ? nếu thõa thì là gì : còn k thì là gì

    // value === "" ? (message = "Vui lòng nhập thông tin") : (message = "");
    message = value === "" ? "Vui lòng nhập " + name : "";
    let { manvValid, tennvValid, emailValid } = this.state;

    switch (name) {
      case "manv":
        manvValid = message !== "" ? false : true;
        if (value !== "" && value.length < 4) {
          manvValid = false;
          message = "Bạn phải nhập lớn hơn 3 kí tự";
        }
        break;
      case "tennv":
        tennvValid = message !== "" ? false : true;
        break;
      case "email":
        emailValid = message !== "" ? false : true;
        if (
          value !== "" &&
          !value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)
        ) {
          message = "Email không hợp lệ";
          emailValid = false;
        }
        // 2 dòng sét trong if thì phải bỏ vào cặp ngoặc nhọn
        // 1 thì k cần
        break;
    }

    this.setState(
      {
        errors: { ...this.state.errors, [name]: message },
        manvValid,
        tennvValid,
        emailValid,
      },
      () => {
        this.validationForm();
        console.log(this.state);
      }
    );
  };

  validationForm = () => {
    const { manvValid, tennvValid, emailValid } = this.state;
    this.setState({
      formValid: manvValid && tennvValid && emailValid,
      //Submit chỉ hiện khi 3 thằng này đều true
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    console.log(this.state.values);
    this.props.dispatch({
      type: "SUBMIT",
      payload: this.props.item,
    });
  };

  static getDerivedStateFromProps(newProps, currentState) {
    if (newProps.userEdit && newProps.userEdit.id !== currentState.values.id) {
      let newState = {
        ...currentState,
        values: newProps.userEdit,
        formValid: true,
        manvValid: true,
        tennvValid: true,
        emailValid: true,
      };
      return newState;
    }
    return null;
  }

  //   onBlur={this.handleErrors}
  //Khi ng dùng nhập vào ô gõ mà họ k gõ mà click ra ngoài thì sự kiện là onBLur
  render() {
    return (
      <div className="container">
        <h3 className="title">*FormValidation</h3>
        <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            <label>Mã Nhân Viên</label>
            <input
              type="text"
              className="form-control"
              name="manv"
              onChange={this.handleOnchange}
              onBlur={this.handleErrors}
              onKeyUp={this.handleErrors}
              value={this.state.values.manv}
            />
          </div>
          {this.state.errors.manv !== "" ? (
            <div className="alert alert-danger">{this.state.errors.manv}</div>
          ) : (
            ""
          )}
          <div className="form-group">
            <label>Tên Nhân Viên</label>
            <input
              type="text"
              className="form-control"
              name="tennv"
              onChange={this.handleOnchange}
              onBlur={this.handleErrors}
              onKeyUp={this.handleErrors}
              value={this.state.values.tennv}
            />
          </div>
          {this.state.errors.tennv !== "" ? (
            <div className="alert alert-danger">{this.state.errors.tennv}</div>
          ) : (
            ""
          )}
          <div className="form-group">
            <label>Email</label>
            <input
              type="email"
              className="form-control"
              name="email"
              onChange={this.handleOnchange}
              onBlur={this.handleErrors}
              onKeyUp={this.handleErrors}
              value={this.state.values.email}
            />
          </div>
          {this.state.errors.email !== "" ? (
            <div className="alert alert-danger">{this.state.errors.email}</div>
          ) : (
            ""
          )}
          <button
            type="submit"
            className="btn btn-success"
            disabled={!this.state.formValid}
          >
            Submit
          </button>
        </form>
        <ListUser />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userEdit: state.formValidationReducer.userEdit,
  };
};

export default connect(mapStateToProps)(FormValidation);
