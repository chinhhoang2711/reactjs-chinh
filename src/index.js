import React from "react";
import ReactDOM from "react-dom";
// import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import { createStore, combineReducers } from "redux";
import { Provider } from "react-redux";
import formReducer from "./UserManagement/redux/reducers/form";
import userList from "./UserManagement/redux/reducers/userlist";
import selectedUser from "./UserManagement/redux/reducers/slectedUser";
import formValidationReducer from "./Form-validation/reducer/reducerVal";

//Tạo reducer tổng (root)
const reducer = combineReducers({
  // Tất cả dữ liệu của Store sẽ được chứa trong đây
  // tenDuLieu: reducerQuanLy
  // ví dụ: cart: cartReducer

  isShow: formReducer,
  // userList: userList,
  //import tên gì thì set lại ở đây, Cố tình đặt giống tên để xóa 1 vế đi
  userList,

  //có tình đặt trùng tên
  selectedUser,

  formValidationReducer,
});

const store = createStore(
  reducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
