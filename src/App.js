import React from "react";

// import Home from "./exercise-1/Home";

// import "./exercise-1/Home/style.css";

// import Home from "./exercise-2/Home";

// import "./App.css";
import Databinding from "./Databinding";
import CarExercise from "./CarExercise";
import MovieExercise from "./MovieExercise";
import DemoParent from "./DemoProps/DemoParent";
import HomePage from "./Bt1/HomePage";
import HomeShopping from "./Shopping/Home";
import ReactForm from "./ReactForm";
import GlassAppOnline from "./BT2-GlassOnl";
import Home from "./UserManagement/Home";
import FormValidation from "./Form-validation";
import LifeCycle from "./Life-Cycle";

function App() {
  return <div>
      <FormValidation/>
      {/* <LifeCycle/> */}
  </div>;
}

export default App;
